package com.sorting;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

public class ReadWrite {
	
	public List<String> read() {
		List<String> savedNumbers = new ArrayList<>();
		ServletContext ctx = (ServletContext) FacesContext
	            .getCurrentInstance().getExternalContext().getContext();
	    String path = ctx.getRealPath("/");
	    
	    try(BufferedReader br = new BufferedReader(new FileReader(path + "numbers.txt"))) {
	        String line = br.readLine();
	        while (line != null) {
	        	savedNumbers.add(line);
	        	line = br.readLine();
	        }
	    } catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	    return savedNumbers;
	}
}
