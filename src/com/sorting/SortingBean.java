package com.sorting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

@ManagedBean
@SessionScoped
public class SortingBean {

	private String number;
	private String sorted = "";
	private int count;
	private List<String> savedNumbers;
	private ReadWrite readWrite = new ReadWrite();
	
	public void sort() {
		
		long startTime = System.currentTimeMillis();
		char[] charArray = number.toCharArray();

		for(int i=0;i<(charArray.length-1);i++){
	        for(int j=i+1;j>0;j--){
	            if(charArray[j]<charArray[j-1]){
	            	count++;
	                char temp=charArray[j-1];
	                charArray[j-1]=charArray[j];
	                charArray[j]=temp;
	            }
	        }
	    }
        
		sorted = String.valueOf(charArray);
		
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		
		System.out.println("Elapsed Time : " + elapsedTime);
		System.out.println("Count : " + count);
		
		ServletContext ctx = (ServletContext) FacesContext
	            .getCurrentInstance().getExternalContext().getContext();
	    String path = ctx.getRealPath("/");
	    
	    try {
			
			String filename= path + "numbers.txt";
		    FileWriter fw = new FileWriter(filename,true); 
		    fw.write(sorted + " - Elapsed Time : " + elapsedTime + " - Platsbyten : " + count);
		    fw.write(System.getProperty("line.separator")); 
		    fw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	    savedNumbers = readWrite.read();
	}
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getSorted() {
		return sorted;
	}

	public void setSorted(String sorted) {
		this.sorted = sorted;
	}

	public List<String> getSavedNumbers() {
		return savedNumbers;
	}

	public void setSavedNumbers(List<String> savedNumbers) {
		this.savedNumbers = savedNumbers;
	}
}
